//	gutenget.cpp
//
//	Copyright (C) 2009 Mattia Milleri
//
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <fstream>		// Per strcpy
#include <stdio.h>		// Per il remove()
#include <curl/curl.h>	// Per il download
#include <bzlib.h>		// Per i file compressi
using namespace std;

#define VERSION "0.1"	// Versione del programma

bool DownloadFile( char* url, char* filename );
bool ExtractFile( char* input, char* output );

int main() {
	char url[100], filename[100]; 	// url del file da scaricare e nome del file
	char archive[100], data[100];	// nome del file compresso e estratto
	bool downloadSuccess, extractSuccess;

	printf( "gutenget %s\n", VERSION );
	// Controlla se c'è ancora il file catalog.rdf
	FILE *checkfile = fopen( "catalog.rdf", "r" );
	if ( checkfile != NULL ) {
		// Se il file è presente lo elimina
		fclose( checkfile );
		remove( "catalog.rdf" );
		printf( "File removed!\n");
	}
	// Scarica il catalogo dal sito
	//system("wget http://www.gutenberg.org/feeds/catalog.rdf.bz2");
	strcpy( url, "http://www.gutenberg.org/feeds/catalog.rdf.bz2" );
	strcpy( filename, "catalog.rdf.bz2" );
	downloadSuccess = DownloadFile( url, filename );
	if ( downloadSuccess )
		printf( "Download successful!\n" );
	else
		printf( "Download failed!\n" );

	// Scompatta l'archivio del catalogo
	//system("bunzip2 -vv catalog.rdf.bz2");
	strcpy( archive, "catalog.rdf.bz2" );
	strcpy( data, "catalog.rdf" );
	extractSuccess = ExtractFile( archive, data );
	if ( extractSuccess )
		printf ( "Extraction successful!\n" );
	else
		printf ( "Extraction failed!\n" );

	return 0;
}

// Scarica file da 'url' salvandolo come 'filename'
bool DownloadFile( char* url, char* filename ) {
	CURL *curl;
	CURLcode res;
	FILE *data;
	curl = curl_easy_init();

	data = fopen( filename, "w" );

	curl_easy_setopt( curl, CURLOPT_URL, url );
	curl_easy_setopt( curl, CURLOPT_WRITEDATA, data );
	res = curl_easy_perform( curl );

	// Anche se non trova il file, con il fatto che i siti di adesso
	// restituiscono comunque una pagina, non da errore.
	if ( res != CURLE_OK ) {
		printf( "Download error: %d\n", res );
		return false;
	}

	curl_easy_cleanup( curl );
	fclose( data );

	return true;
}

// Estrae il file 'output' dall'archivio 'input'
bool ExtractFile( char* inputName, char* outputName ) {
	FILE 	*archive, *output;
	BZFILE 	*bz2file;
	int     nBuf;
	char    buf[ 10 ];
	int     bzerror;

	archive = fopen( inputName,  "r" );
	output 	= fopen( outputName, "w" );
	bz2file = BZ2_bzReadOpen( &bzerror, archive, 0, 0, NULL, 0 );

	// Finchè non da errore, continua a leggere dal file
	bzerror = BZ_OK;
	while ( bzerror == BZ_OK ) {
	  nBuf = BZ2_bzRead ( &bzerror, bz2file, buf, 10 );
	  if ( bzerror == BZ_OK || bzerror == BZ_STREAM_END ) {
	    fprintf( output, buf );
	  } else {
		  return false;
	  }
	}

	BZ2_bzReadClose ( &bzerror, bz2file );
	fclose( archive );
	fclose( output );

	return true;
}
